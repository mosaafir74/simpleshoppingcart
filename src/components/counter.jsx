import React, { Component } from 'react';

class Counter extends Component {
// state ={
//     value: this.props.counter.value
// }


formatCount (){
    //const { value } = this.props.counter;
    return this.props.counter.value === 0 ? "zero": this.props.counter.value;
}


    render() { 
        return ( 
            <div>
                <span className={ this.getBadgeClass() }>{this.formatCount()}</span>
                <button onClick = {() => this.props.onIncrement(this.props.counter)} className="btn btn-secondary btn-sm m-2">increment</button>
                <button onClick={() =>  this.props.onDelete(this.props.counter.id)} className="btn btn-danger btn-sm m-2">Delete</button>
            </div>
        );
    }

    getBadgeClass() {
        let classes = "badge badge-sm m-2 badge-";
        classes += this.props.counter.value === 0 ? "warning" : "primary";
        return classes;
    }
}

export default Counter;